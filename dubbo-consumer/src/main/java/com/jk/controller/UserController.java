package com.jk.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.User;
import com.jk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class UserController {
    @Reference
    private UserService userService;

    @RequestMapping("/")
    public String index() {
        return "redirect:/list";
    }

    @RequestMapping("/sele")
    public String sele(Model model,User user) {
        List<User> list = userService.seleUser(user);
        model.addAttribute("list", list);
        return "user/show";
    }

    @GetMapping("toAdd")
    public String toAdd() {
        return "user/userAdd";
    }
    @RequestMapping("/add")
    public String add(User user) {
        userService.save(user);
        return "redirect:/sele";
    }
    @RequestMapping("/toEdit")
    public String toEdit(Model model, Integer id) {
        User user=userService.findUserById(id);
        model.addAttribute("user", user);
        return "user/userEdit";
    }
    @RequestMapping("/edit")
    public String edit(User user) {
        userService.edit(user);
        return "redirect:/sele";
    }
    @RequestMapping("/delete")
    public String delete(Integer id) {
        userService.delete(id);
        return "redirect:/sele";
    }

    public  void  user(){

        System.out.println(56565656);
        System.out.println(899999);
        System.out.println(123422);
        System.out.println(987652);
        System.out.println(444444444);


    }

}