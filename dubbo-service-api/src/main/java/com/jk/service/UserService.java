package com.jk.service;

import com.jk.model.User;

import java.util.List;

public interface UserService {

    List<User> seleUser(User user);

    public void save(User user);

    public User findUserById(Integer id);

    void edit(User user);

    void delete(Integer id);
}
