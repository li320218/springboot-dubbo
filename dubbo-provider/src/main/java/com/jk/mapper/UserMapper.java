package com.jk.mapper;

import com.jk.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserMapper {
    @SelectProvider(type = UserProvider.class, method = "selectOrderWithWhere")
    List<User> seleUser(User user);
@Insert("insert into bbbb.jk_student (id,name,age,sex) values (#{name},#{age},#{sex})")
    void save(User user);
@Select("select * from bbbb.jk_student where id=#{id}")
    User findUserById(Integer id);
@Update("update jk_student set name=#{name},age=#{age},sex=#{sex} where id=#{id}")
    void edit(User user);
@Delete("delete from jk_student where id=#{id}")
    void delete(Integer id);
}
