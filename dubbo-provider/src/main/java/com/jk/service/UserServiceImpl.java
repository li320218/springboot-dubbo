package com.jk.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.UserMapper;
import com.jk.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
@Service(interfaceClass = UserService.class)
@Component
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> seleUser(User user) {

        return userMapper.seleUser(user);
    }

    @Override
    public void save(User user) {
        userMapper.save(user);
    }

    @Override
    public User findUserById(Integer id) {
        return userMapper.findUserById(id);
    }

    @Override
    public void edit(User user) {
        userMapper.edit(user);
    }

    @Override
    public void delete(Integer id) {
        userMapper.delete(id);
    }


}
